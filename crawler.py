import requests
from bs4 import BeautifulSoup
import csv


def get_html(url):
    r = requests.get(url)  # Response obj
    return r.text          # возвращает HTML-код страницы (url)

def get_all_links(html):
    soup = BeautifulSoup(html, 'lxml')

    main_news = soup.find('div', class_='main__col-main').find('div', class_='main__col-main__inner')   # Soup obj (главная новость)
    main_a = main_news.find('a', class_='main__big').get('href')                                        
    links = []
    links.append(main_a)   # Добавляем ссылку на главную новость

    news = soup.find('div', class_='main__col-list').find_all('div', class_='main-feed__item')          # Soup obj (выборка остальных новостей)
    
    for item in news:
        a = item.find('a', class_='main-feed__link').get('href')
        links.append(a)    # Добавляем ссылки на остальные новости

    return links


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')

    try:
        title = soup.find('span', class_='js-slide-title').text.strip()                 # Получаем заголовок
    except:
        title = ''

    try:
        pub_date = soup.find('span', class_='article__header__date').text.strip()       # Получаем дату
    except:
        pub_date = ''

    try:
        link = soup.find('div', class_='article__content').get('data-io-article-url')   # Получаем url
    except:
        link = ''

    try:
        p = soup.find('div', class_='article__text').find_all('p')                      # Получаем все параграфы
        result = ''
        
        for item in p:
            result += item.text.strip()                                                 # Клеим текст каждого параграфа в общий
        text = result
    except:
        text = ''

    data = {'title': title,
            'pub_date': pub_date,
            'link': link,
            'text': text}

    return data


def write_txt(data):
    f = open('rbcnews.txt', 'a', encoding='utf8')
    result = 'Заголовок: \n {} \nДата публикации: \n {} \nТекст: \n {} \nСсылка: \n {} \n\n'.format(data['title'],    # Запись в файл с форматированием.
                                                                                                    data['pub_date'],
                                                                                                    data['text'],
                                                                                                    data['link'])
    f.write(result)
    f.close

def main():     
    url = 'https://www.rbc.ru/'

    all_links = get_all_links(get_html(url))

    for url in all_links:
        html = get_html(url)
        data = get_page_data(html)
        print(data['title'])
        print(data['pub_date'])
        print(data['link'])
        print()
        write_txt(data)


if __name__ == '__main__':
    main()